//basket & shopping cart

function addToCart(item) {
    cartArray.push(item);
    document.querySelector('.main-header').classList.replace('not-fixed', 'fixed');
    cartSum();
}

function removeFromCart(item) {
    console.log(item);
    item.remove();
    if (cartArray.length === 1) {
        document.querySelector('.product-shop ul .empty-state').classList.remove('hide');
        document.querySelector('.basket-footer').classList.add('hide');
    }
    cartArray.splice(0, 1);
    cartSum();
}

function cartSum() {
    let sum = 0;
    let count = 0;
    cartArray.forEach(function (item) {
        sum += item.price;
        count++;
    });
    document.querySelector('.price-sum').innerHTML = sum;
    document.querySelector('.basket-count span').innerHTML = count;
}

function openCart() {
    let item = document.querySelector('.main-header');
    item.classList.remove('top');
    item.classList.replace('not-fixed', 'fixed');
    if (document.querySelector('.basket').className.includes('active')) {
        document.querySelector('.basket').classList.remove('active');
        document.querySelector('.basket-modal').classList.remove('show');
        if (scrollY < 50) {
            item.classList.add('top');
        }
    } else {
        document.querySelector('.basket').classList.add('active');
        document.querySelector('.basket-modal').classList.add('show');
    }
}

function addToBasketModal(item) {
    updateCartModalPositioning();
    let newLi = document.createElement('li');
    let newImg = document.createElement('img');
    newImg.src = item.image;
    let newH = document.createElement('h3');
    newH.innerHTML = item.title;
    let newP = document.createElement('p');
    newP.innerHTML = item.price;
    let newI = document.createElement('i');
    newI.classList.add('material-icons');
    newI.innerHTML = 'close';
    newLi.appendChild(newImg);
    newLi.appendChild(newH);
    newLi.appendChild(newP);
    newLi.appendChild(newI);
    newI.addEventListener('click', function () {
        if (document.querySelector('.list-item')) removeFromBasket(this.parentNode);
        removeFromCart(this.parentNode);
    });
    document.querySelector('.product-shop ul').appendChild(newLi);
    document.querySelector('.product-shop ul .empty-state').classList.add('hide');
    document.querySelector('.basket-footer').classList.remove('hide');
    addToCart(item);
}

function updateCartModalPositioning() {
    let modal = document.querySelector('.basket-modal');
    let position = cartArray.length;
    let newStyle = possibleHeights[position] * -1 + 'px';
    modal.style.top = newStyle;
}

function checkCartStatus() {
    if (array.length === 0) {
        document.querySelector('.item-list .list-main .empty-state').classList.add('show');
        document.querySelector('.list-footer button').classList.add('disabled');
        //form section removal
        document.querySelector('.forms').classList.add('hide');
        document.querySelector('.item-list .list-container .list-footer .proceed').classList.add('hide');
        document.querySelector('.item-list .list-container .list-footer .links').classList.remove('hide');
        //payment section removal
        document.querySelector('.payment').classList.add('hide');
        document.querySelector('.forms .form-container .form-footer .proceed').classList.add('hide');
        document.querySelector('.forms .form-container .form-footer .links').classList.remove('hide');
    }
}

function removeFromBasket(item) {
    document.querySelectorAll('.product-shop li').forEach(function (it, index) {
        if (item === it) {
            array.splice(0, 1);
            document.querySelectorAll('.list-item')[index].remove();
            checkCartStatus();
            calcCartSum();
        }
    });
}

function calcCartSum() {
    let price = 0;
    let items = document.querySelectorAll('.list-item .price');
    items.forEach(function (item) {
        price += Number(item.innerHTML);
    });
    price = Number(price.toFixed(2));
    let iva = price * .2;
    iva = Number(iva.toFixed(2));
    let total = price + iva;
    total = Number(total.toFixed(2));
    document.querySelector('.list-subtotal p').innerHTML = price;
    document.querySelector('.payment-item-1 .results .result-subtotal').innerHTML = price;
    document.querySelector('.payment-item-1 .results .result-iva').innerHTML = iva;
    document.querySelector('.payment-item-1 .results .result-total').innerHTML = total;
}

document.querySelector('#search-input').addEventListener('change', function () {
    let txt = this.value;
    let filteredArray = [];
    if (txt != '') {
        filteredArray = products.filter(function (pdt) {
            if (pdt.type.includes(txt) || pdt.title.includes(txt) || pdt.description.includes(txt)) {
                return pdt;
            }
        })
        addSearchSection(filteredArray, txt);
    }
})

function addSearchSection(array, text) {

    //clean main tag
    let mainTag = document.querySelector('main');
    mainTag.innerHTML = '';
    mainTag.classList.remove('integrated');
    document.querySelector('footer').classList.remove('integrated');

    //create breadcrumb section
    let bcSection = document.createElement('section');
    bcSection.classList.add('breadcrumb');
    let ulTag = document.createElement('ul');
    let liTag1 = document.createElement('li');
    let aTag1 = document.createElement('a');
    aTag1.href = './index.html';
    aTag1.innerHTML = 'Home';
    let liTag2 = document.createElement('li');
    let aTag2 = document.createElement('a');
    aTag2.href = '';
    aTag2.innerHTML = 'Search results';
    liTag1.appendChild(aTag1);
    liTag2.appendChild(aTag2);
    ulTag.appendChild(liTag1);
    ulTag.appendChild(liTag2);
    bcSection.appendChild(ulTag);
    mainTag.appendChild(bcSection);

    //create new section
    let sectionTag = document.createElement('section');
    sectionTag.classList.add('products');
    let h2Tag = document.createElement('h2');
    let pTag = document.createElement('p');
    pTag.classList.add('all-products');
    let emptyStateTag = document.createElement('div');
    emptyStateTag.classList.add('empty-state');
    let imgTag = document.createElement('img');
    imgTag.src = './images/illustrations/nodata.svg';
    imgTag.alt = 'No data';
    let pEmptyStateTag = document.createElement('p');
    pEmptyStateTag.classList.add('empty-state-text');
    let container1Tag = document.createElement('div');
    container1Tag.classList.add('product-container', 'page-1');
    let container2Tag = document.createElement('div');
    container2Tag.classList.add('product-container', 'page-2');
    let container3Tag = document.createElement('div');
    container3Tag.classList.add('product-container', 'page-3');
    let paginationTag = document.createElement('div');
    paginationTag.classList.add('pagination');

    if (array.length === 0) {
        pTag.innerHTML = `0 products!`;
    } else if (array.length === 1) {
        pTag.innerHTML = `1 product!`;
    } else {
        pTag.innerHTML = `${array.length} products!`;
    }
    h2Tag.innerHTML = `Search results for: <span class="text-searched">${text}</span>`;
    pEmptyStateTag.innerHTML = `No products found!`;

    //build childs & parents
    sectionTag.appendChild(h2Tag);
    sectionTag.appendChild(pTag);
    emptyStateTag.appendChild(imgTag);
    emptyStateTag.appendChild(pEmptyStateTag);
    sectionTag.appendChild(emptyStateTag);
    sectionTag.appendChild(container1Tag);
    sectionTag.appendChild(container2Tag);
    sectionTag.appendChild(container3Tag);
    sectionTag.appendChild(paginationTag);
    mainTag.appendChild(sectionTag);

    createListCopy(array, 'none', 'none');

}
function createPaginationCopy() {
    let noDataStatus = true;
    document.querySelectorAll('.product-container').forEach(function (item, index) {
        if (item.childElementCount > 0) {
            noDataStatus = false;
            let pageNo = index + 1;
            let divTag = document.createElement('div');
            let divClass = 'page-' + pageNo;
            divTag.classList.add(divClass);
            item.classList.remove('current', 'hidden');
            if (index === 0) {
                divTag.classList.add('current');
                item.classList.add('current');
            } else {
                item.classList.add('hidden');
            }
            divTag.innerHTML = pageNo;
            divTag.addEventListener('click', function () {
                document.querySelector('.product-container.current').classList.replace('current', 'hidden');
                document.querySelector('.' + divClass).classList.replace('hidden', 'current');
                document.querySelector('.pagination .current').classList.remove('current');
                this.classList.add('current');
            });
            document.querySelector('.pagination').appendChild(divTag);
        }
    });
    if (noDataStatus) document.querySelector('.products .empty-state').style.display = 'block';
    else document.querySelector('.products .empty-state').style.display = 'none';
    window.scrollTo(0, 0);
}

function createListCopy(array, chipValue, priceValue) {
    array.forEach(function (item) {
        let aTag = document.createElement('a');
        aTag.href = './product.html';
        let artTag = document.createElement('article');
        let imgTag = document.createElement('img');
        imgTag.setAttribute('src', item.image);
        let descTag = document.createElement('p');
        descTag.classList.add('description');
        descTag.innerHTML = item.title;
        let priceTag = document.createElement('p');
        priceTag.classList.add('price');
        priceTag.innerHTML = item.price;
        artTag.appendChild(imgTag);
        let chipsTag = document.createElement('div');
        chipsTag.classList.add('chips');
        if (item.sale) {
            let saleChip = document.createElement('div');
            saleChip.classList.add('sale');
            saleChip.innerHTML = 'sale';
            chipsTag.appendChild(saleChip);
        }
        if (item.bundle) {
            let bundleChip = document.createElement('div');
            bundleChip.classList.add('bundle');
            bundleChip.innerHTML = 'bundle';
            chipsTag.appendChild(bundleChip);
        }
        artTag.appendChild(chipsTag);
        artTag.appendChild(descTag);
        artTag.appendChild(priceTag);
        aTag.appendChild(artTag);
        let typ = '.products .product-container.page-1';
        let container = document.querySelector(typ);
        let selector_page2 = '.products .product-container.page-2';
        let container_page2 = document.querySelector(selector_page2);
        let selector_page3 = '.products .product-container.page-3';
        let container_page3 = document.querySelector(selector_page3);
        let priceFilterStatus = priceBooleanCopy(priceValue, item.price);
        let chipFilterStatus = chipBooleanCopy(chipValue, item.sale, item.bundle);
        if (chipFilterStatus && priceFilterStatus) {
            if (container && container.childElementCount < productLimitPerPage) {
                container.appendChild(aTag);
            } else if (container_page2 && container_page2.childElementCount < productLimitPerPage) {
                container_page2.appendChild(aTag);
            } else if (container_page3 && container_page3.childElementCount < productLimitPerPage) {
                container_page3.appendChild(aTag);
            }
        }
    });
    createPaginationCopy();
}

function priceBooleanCopy(filter, value) {
    if (filter === 'price-xs' && value <= 199) {
        return true;
    } else if (filter === 'price-s' && value > 199 && value <= 799) {
        return true;
    } else if (filter === 'price-m' && value > 799 && value <= 1999) {
        return true;
    } else if (filter === 'price-l' && value > 1999 && value <= 4999) {
        return true;
    } else if (filter === 'price-xl' && value > 4999) {
        return true;
    } else if (filter === 'none') {
        return true;
    }
    return false;
}

function chipBooleanCopy(filter, sale, bundle) {
    if (filter === 'none') {
        return true;
    } else if (filter === 'any' && (sale || bundle)) {
        return true;
    } else if (filter === 'both' && sale && bundle) {
        return true;
    } else if (filter === 'sale' && sale) {
        return true;
    } else if (filter === 'bundle' && bundle) {
        return true;
    } else if (filter === 'nochip' && !bundle && !sale) {
        return true;
    }
    return false;
}