const gallery = [
    // variable to save all the photos & info
    {
        imgUrl: './images/macbook.jpg',
        imgTitle: 'The incredibly thin MacBook Air',
        imgDescription: 'MacBook Air now features the new Magic Keyboard',
        imgActive: false
    },
    {
        imgUrl: './images/in_ear_phones.jpg',
        imgTitle: 'All-day audio on the go',
        imgDescription: "Your time is limited, so don't waste it living someone else's life",
        imgActive: false
    },
    {
        imgUrl: './images/mouse.jpg',
        imgTitle: 'Simple gestures on the smooth',
        imgDescription: 'To design something really well, you have to get it',
        imgActive: false
    }
];

function nextImage(id, check) {
    let activeTag = document.querySelector('.slideshow .main-photo .active');
    if (!activeTag.className.includes('switching')) {
        activeTag.classList.add('switching');
        let prevId = 0;
        gallery.forEach(function (item, index) {
            if (item.imgActive && index != gallery.length - 1) {
                item.imgActive = false;
                prevId = index;
                if (!check) {
                    id = index + 1;
                }
            } else if (item.imgActive && index === gallery.length - 1) {
                item.imgActive = false;
                prevId = index;
                if (!check) {
                    id = 0;
                }
            }
        });
        if (prevId != id) {
            let prevSquareTag = "[squareno='" + prevId + "']";
            let nextSquareTag = "[squareno='" + id + "']";
            document.querySelector(prevSquareTag).classList.remove('current');
            document.querySelector(nextSquareTag).classList.add('current');
            let prevIdTag = ".active div[imgNo='" + prevId + "']";
            let nextIdTag = ".active div[imgNo='" + id + "']";
            if (check && prevId > id) {
                document.querySelector(prevIdTag).classList.add('plus');
                document.querySelector(prevIdTag).classList.remove('minus', 'equal', 'show');
            } else {
                document.querySelector(prevIdTag).classList.remove('plus', 'equal', 'show');
                document.querySelector(prevIdTag).classList.add('minus');
            }
            document.querySelector(nextIdTag).classList.remove('plus', 'minus');
            document.querySelector(nextIdTag).classList.add('equal', 'show');
            gallery[id].imgActive = true;
            activeTag.classList.remove('switching');
            alignWidth(id);
            if (id === gallery.length - 1) {
                stopNext = true;
                document.querySelector('#next-btn').classList.add('disabled');
                document.querySelector('#prev-btn').classList.remove('disabled');
            } else if (id === 0) {
                stopPrev = true;
                document.querySelector('#prev-btn').classList.add('disabled');
                document.querySelector('#next-btn').classList.remove('disabled');
            } else {
                stopNext = false;
                document.querySelector('#next-btn').classList.remove('disabled');
                document.querySelector('#prev-btn').classList.remove('disabled');
            }
        } else {
            activeTag.classList.remove('switching');
        }
    }
}

function prevImage() {
    let activeTag = document.querySelector('.slideshow .main-photo .active');
    if (!activeTag.className.includes('switching')) {
        activeTag.classList.add('switching');
        let id;
        let prevId = 0;
        gallery.forEach(function (item, index) {
            if (item.imgActive && index != 0) {
                prevId = index;
                item.imgActive = false;
                id = index - 1;
            } else if (item.imgActive && index === 0) {
                prevId = index;
                item.imgActive = false;
                id = gallery.length - 1;
            }
        });
        let prevSquareTag = "[squareno='" + prevId + "']";
        let nextSquareTag = "[squareno='" + id + "']";
        document.querySelector(prevSquareTag).classList.remove('current');
        document.querySelector(nextSquareTag).classList.add('current');
        let prevIdTag = ".active div[imgNo='" + prevId + "']";
        let nextIdTag = ".active div[imgNo='" + id + "']";

        document.querySelector(prevIdTag).classList.remove('minus', 'equal', 'show');
        document.querySelector(prevIdTag).classList.add('plus');
        document.querySelector(nextIdTag).classList.remove('minus', 'plus');
        document.querySelector(nextIdTag).classList.add('equal', 'show');
        gallery[id].imgActive = true;
        activeTag.classList.remove('switching');
        alignWidth(id);

        if (id === 0) {
            stopPrev = true;
            document.querySelector('#prev-btn').classList.add('disabled');
            document.querySelector('#next-btn').classList.remove('disabled');
        } else {
            stopPrev = false;
            document.querySelector('#prev-btn').classList.remove('disabled');
            document.querySelector('#next-btn').classList.remove('disabled');
        }
    }
}

function alignWidth(id) {
    gallery.forEach(function (item, index) {
        let selector = "[imgno='" + index + "']";
        if (index < id) {
            document.querySelector(selector).classList.add('minus');
            document.querySelector(selector).classList.remove('equal', 'plus');
            document.querySelector(selector).classList.remove('show');
        } else if (index > id) {
            document.querySelector(selector).classList.add('plus');
            document.querySelector(selector).classList.remove('equal', 'minus');
            document.querySelector(selector).classList.remove('show');
        }
    });
}

document.querySelector('#next-btn i').addEventListener('click', function () {
    if (!this.parentElement.className.includes('disabled')) {
        nextImage();
    }
})

document.querySelector('#prev-btn i').addEventListener('click', function () {
    if (!this.parentElement.className.includes('disabled')) {
        prevImage();
    }
})

document.querySelector('.new-arrivals').addEventListener('click', function () {
    window.location.href = './bundles.html';
});

gallery.forEach(function (item, index) {
    let url = "url('" + gallery[index].imgUrl + "')";
    let mainPhotoTag = document.createElement('div');
    let photoSquare = document.createElement('div');
    let h1Tag = document.createElement('h1');
    let pTag = document.createElement('p');
    let infoContainer = document.createElement('div');
    mainPhotoTag.style.backgroundImage = url;
    mainPhotoTag.setAttribute('imgNo', index);
    mainPhotoTag.classList.add('photo');
    h1Tag.innerHTML = gallery[index].imgTitle;
    pTag.innerHTML = gallery[index].imgDescription;
    infoContainer.classList.add('container');
    infoContainer.appendChild(h1Tag);
    infoContainer.appendChild(pTag);
    mainPhotoTag.appendChild(infoContainer);
    photoSquare.setAttribute('squareNo', index);
    photoSquare.addEventListener('click', function () {
        if (!gallery[index].imgActive) nextImage(index, true);
    });
    if (index === 0) {
        // first image initialization
        mainPhotoTag.classList.add('show');
        photoSquare.classList.add('current');
        item.imgActive = true;
        stopPrev = true;
        document.querySelector('#prev-btn').classList.add('disabled');
    }
    document.querySelector('.bottom').appendChild(photoSquare);
    document.querySelector('.main-photo .active').appendChild(mainPhotoTag);
});