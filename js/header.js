function searchIn() {
	document.querySelectorAll('.nav-utility li a').forEach(function(item) {
		item.classList.add('hide');
	});
	document.querySelector('.search-icon').classList.add('open');
	document.querySelector('#search-input').classList.add('open');
	document.querySelector('#search-input').focus();
}

function searchOut() {
	document.querySelectorAll('.nav-utility li a').forEach(function(item) {
		item.classList.remove('hide');
	});
	document.querySelector('.search-icon').classList.remove('open');
	document.querySelector('#search-input').classList.remove('open');
}

// document.querySelector('#search-input').addEventListener('focusout', function() {
// 	searchOut();
// });

document.querySelector('.search-icon').addEventListener('click', function(index) {
	if (!this.className.includes('open')) searchIn();
	else searchOut();
});

document.querySelector('.menu-icon .menu').addEventListener('click', function() {
	document.querySelector('.menu-container nav').classList.replace('hide', 'show');
	document.querySelector('.menu-container').classList.add('overlay');
	document.querySelector('body').classList.add('overflow');
});

document.querySelector('.side-menu .close').addEventListener('click', function() {
	document.querySelector('.menu-container nav').classList.replace('show', 'hide');
	document.querySelector('.menu-container').classList.remove('overlay');
	document.querySelector('body').classList.remove('overflow');
});

document.querySelector('.basket').addEventListener('click', function() {
	updateCartModalPositioning();
	openCart();
});

document.querySelector('.basket').addEventListener('focusout', function() {
	if (this.className.includes('active')) {
		this.classList.remove('active');
		document.querySelector('.basket-modal').classList.remove('show');
	} else {
		this.classList.add('active');
		document.querySelector('.basket-modal').classList.add('show');
	}
});

document.onscroll = function() {
	// console.log(scrollY)
	let item = document.querySelector('.main-header');
	let newScrollStatus = scrollY;
	let scrollUp = function() {
		if (newScrollStatus < scrollStatusNum) return true;
		else return false;
	};
	if (!scrollUp() && newScrollStatus > 50) {
		item.classList.remove('top');
		item.classList.remove('fixed');
		item.classList.add('not-fixed');
	}
	if (scrollUp()) {
		if (newScrollStatus < 50) {
			item.classList.replace('not-fixed', 'top');
			item.classList.replace('fixed', 'top');
		} else {
			item.classList.replace('not-fixed', 'fixed');
		}
	}
	scrollStatusNum = newScrollStatus;
	document.querySelector('.basket').classList.remove('active');
	document.querySelector('.basket-modal').classList.remove('show');
};

document.addEventListener('click', function(evt) {
	// console.log(evt.target)
	let modal = document.querySelector('.basket-modal');
	let clickedElement = evt.target;
	if (
		!document.querySelector('.basket').contains(clickedElement) &&
		!modal.contains(clickedElement) &&
		clickedElement.parentNode.parentNode != null
	) {
		document.querySelector('.basket').classList.remove('active');
		document.querySelector('.basket-modal').classList.remove('show');
		updateCartModalPositioning();
	}
});
