let sect = document.querySelector('.single-product');
let imgUrl = "url('" + product.image + "')";
document.querySelector('.single-product .product-image').style.backgroundImage = imgUrl;
document.querySelector('.single-product .product-info h2').innerHTML = product.title;
document.querySelector('.single-product .product-info .price').innerHTML = product.price;
let newLi_type = document.createElement('li');
let newA_type = document.createElement('a');
newA_type.href = './' + product.type + '.html';
newA_type.innerHTML = product.type;
newLi_type.appendChild(newA_type);
let newLi = document.createElement('li');
let newA = document.createElement('a');
newA.href = '#';
newA.innerHTML = product.title;
newLi.appendChild(newA);
document.querySelector('.breadcrumb ul').appendChild(newLi_type);
document.querySelector('.breadcrumb ul').appendChild(newLi);

document.querySelectorAll('.options ul li').forEach(function(item) {
	item.addEventListener('click', function() {
		if (!this.className.includes('selected')) {
			if (document.querySelector('.options ul .selected')) {
				document.querySelector('.options ul .selected').classList.remove('selected');
			}
			document.querySelector('.product-info button').classList.remove('disabled');
			this.classList.add('selected');
		}
	});
});

document.querySelectorAll('.more-images .square').forEach(function(item) {
	item.addEventListener('click', function() {
		let newUrl = item.style.backgroundImage;
		let oldUrl = document.querySelector('.single-product .product-image').style.backgroundImage;
		document.querySelector('.single-product .product-image').style.backgroundImage = newUrl;
		item.style.backgroundImage = oldUrl;
	});
});

document.querySelector('.add-to-cart').addEventListener('click', function() {
	if (!this.className.includes('disabled')) addToBasketModal(product);
});
