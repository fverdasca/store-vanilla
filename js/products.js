let gaming = 0;
let social = 0;
let work = 0;

products.forEach(function(item) {
	if (item.type === 'gaming') gaming += 1;
	else if (item.type === 'social') social += 1;
	else if (item.type === 'work') work += 1;
});

createList('none', 'none');

document.querySelector('.chip-filter').addEventListener('change', function() {
	if (this.value != 'none') this.classList.add('filtered');
	else this.classList.remove('filtered');
	removeList();
	createList(getChipValue(), getPriceValue());
});

document.querySelector('.price-filter').addEventListener('change', function() {
	if (this.value != 'none') this.classList.add('filtered');
	else this.classList.remove('filtered');
	removeList();
	createList(getChipValue(), getPriceValue());
});

function getChipValue() {
	return document.querySelector('.chip-filter').value;
}

function getPriceValue() {
	return document.querySelector('.price-filter').value;
}

function createPagination() {
	let noDataStatus = true;
	document.querySelectorAll('.product-container').forEach(function(item, index) {
		if (item.childElementCount > 0) {
			noDataStatus = false;
			let pageNo = index + 1;
			let divTag = document.createElement('div');
			let divClass = 'page-' + pageNo;
			divTag.classList.add(divClass);
			item.classList.remove('current', 'hidden');
			if (index === 0) {
				divTag.classList.add('current');
				item.classList.add('current');
			} else {
				item.classList.add('hidden');
			}
			divTag.innerHTML = pageNo;
			divTag.addEventListener('click', function() {
				document.querySelector('.product-container.current').classList.replace('current', 'hidden');
				document.querySelector('.' + divClass).classList.replace('hidden', 'current');
				document.querySelector('.pagination .current').classList.remove('current');
				this.classList.add('current');
			});
			document.querySelector('.pagination').appendChild(divTag);
		}
	});
	fallback(noDataStatus);
}

function fallback(boolean) {
	if (boolean) document.querySelector('.products .empty-state').style.display = 'block';
	else document.querySelector('.products .empty-state').style.display = 'none';
}

function removeList() {
	document.querySelectorAll('.product-container a').forEach(function(item) {
		item.remove();
	});
	document.querySelectorAll('.pagination div').forEach(function(item) {
		item.remove();
	});
}

function priceBoolean(filter, value) {
	if (filter === 'price-xs' && value <= 199) {
		return true;
	} else if (filter === 'price-s' && value > 199 && value <= 799) {
		return true;
	} else if (filter === 'price-m' && value > 799 && value <= 1999) {
		return true;
	} else if (filter === 'price-l' && value > 1999 && value <= 4999) {
		return true;
	} else if (filter === 'price-xl' && value > 4999) {
		return true;
	} else if (filter === 'none') {
		return true;
	}
	return false;
}

function chipBoolean(filter, sale, bundle) {
	if (filter === 'none') {
		return true;
	} else if (filter === 'any' && (sale || bundle)) {
		return true;
	} else if (filter === 'both' && sale && bundle) {
		return true;
	} else if (filter === 'sale' && sale) {
		return true;
	} else if (filter === 'bundle' && bundle) {
		return true;
	} else if (filter === 'nochip' && !bundle && !sale) {
		return true;
	}
	return false;
}

function createList(chipValue, priceValue) {
	products.forEach(function(item) {
		let aTag = document.createElement('a');
		aTag.href = './product.html';
		let artTag = document.createElement('article');
		let imgTag = document.createElement('img');
		imgTag.setAttribute('src', item.image);
		let descTag = document.createElement('p');
		descTag.classList.add('description');
		descTag.innerHTML = item.title;
		let priceTag = document.createElement('p');
		priceTag.classList.add('price');
		priceTag.innerHTML = item.price;
		artTag.appendChild(imgTag);
		let chipsTag = document.createElement('div');
		chipsTag.classList.add('chips');
		if (item.sale) {
			let saleChip = document.createElement('div');
			saleChip.classList.add('sale');
			saleChip.innerHTML = 'sale';
			chipsTag.appendChild(saleChip);
		}
		if (item.bundle) {
			let bundleChip = document.createElement('div');
			bundleChip.classList.add('bundle');
			bundleChip.innerHTML = 'bundle';
			chipsTag.appendChild(bundleChip);
		}
		artTag.appendChild(chipsTag);
		artTag.appendChild(descTag);
		artTag.appendChild(priceTag);
		aTag.appendChild(artTag);
		let typ = '.products.' + item.type + ' .product-container.' + item.type;
		let container = document.querySelector(typ);
		let selector_page2 = '.products.' + item.type + ' .product-container.' + item.type + '.page-2';
		let container_page2 = document.querySelector(selector_page2);
		let selector_page3 = '.products.' + item.type + ' .product-container.' + item.type + '.page-3';
		let container_page3 = document.querySelector(selector_page3);
		let priceFilterStatus = priceBoolean(priceValue, item.price);
		let chipFilterStatus = chipBoolean(chipValue, item.sale, item.bundle);
		if (chipFilterStatus && priceFilterStatus) {
			if (container && container.childElementCount < productLimitPerPage) {
				container.appendChild(aTag);
			} else if (container_page2 && container_page2.childElementCount < productLimitPerPage) {
				container_page2.appendChild(aTag);
			} else if (container_page3 && container_page3.childElementCount < productLimitPerPage) {
				container_page3.appendChild(aTag);
			}
		}
	});
	createPagination();
}
