let array = products.filter(function (item) {
    return item.cart;
});

array.forEach(function (item, index) {
    let newLiListItem = document.createElement('li');
    newLiListItem.classList.add('list-item');
    let newUl = document.createElement('ul');
    let newLi1 = document.createElement('li');
    let newImg = document.createElement('img');
    newImg.src = item.image;
    newImg.alt = item.title;
    newLi1.appendChild(newImg);
    let newLi2 = document.createElement('li');
    newLi2.classList.add('item-description');
    let newHtitle = document.createElement('h3');
    newHtitle.innerHTML = item.title;
    let newHdescp = document.createElement('p');
    newHdescp.innerHTML = item.description;
    newLi2.appendChild(newHtitle);
    newLi2.appendChild(newHdescp);
    let newLi3 = document.createElement('li');
    let newHcolor = document.createElement('h4');
    newHcolor.innerHTML = item.color;
    newLi3.appendChild(newHcolor);
    let newLi4 = document.createElement('li');
    let newInput = document.createElement('input');
    newInput.type = 'number';
    newInput.value = 1;
    newInput.min = 0;
    newLi4.appendChild(newInput);
    let newLi5 = document.createElement('li');
    let newHprice = document.createElement('h4');
    newHprice.innerHTML = item.price;
    newHprice.classList.add('price');
    newLi5.appendChild(newHprice);
    let divDelete = document.createElement('div');
    divDelete.classList.add('delete-icon');
    let iIcon = document.createElement('i');
    iIcon.classList.add('material-icons');
    iIcon.innerHTML = 'delete';
    divDelete.appendChild(iIcon);

    newUl.appendChild(newLi1);
    newUl.appendChild(newLi2);
    newUl.appendChild(newLi3);
    newUl.appendChild(newLi4);
    newUl.appendChild(newLi5);
    newUl.appendChild(divDelete);

    newLiListItem.appendChild(newUl);

    document.querySelector('.list-items-ul').appendChild(newLiListItem);

    newInput.addEventListener('change', function () {
        newHprice.innerHTML = item.price * this.value;
        calcCartSum();
    });

    divDelete.addEventListener('click', function () {
        array.splice(0, 1);
        newLiListItem.remove();
        checkCartStatus();
        calcCartSum();
        document.querySelectorAll('.product-shop h3').forEach(function (it, index) {
            if (item.title.toLowerCase() === it.innerText.toLowerCase()) {
                removeFromCart(it.parentNode);
            }
        });
    });
    addToBasketModal(item);
});

calcCartSum();

function proceedToStep2() {
    document.querySelector('.forms').classList.remove('hide');
    document.querySelector('.item-list .list-container .list-footer .proceed').classList.remove('hide');
    document.querySelector('.item-list .list-container .list-footer .links').classList.add('hide');
    // document.querySelector('.item-list .list-container .list-footer .proceed').scrollIntoView();
}

function proceedToStep3() {
    document.querySelector('.payment').classList.remove('hide');
    document.querySelector('.forms .form-container .form-footer .proceed').classList.remove('hide');
    document.querySelector('.forms .form-container .form-footer .links').classList.add('hide');
    // document.querySelector('.forms .form-container .form-footer .proceed').scrollIntoView();
}

function returnToStep2() {
    document.querySelector('.payment').classList.add('hide');
    document.querySelector('.forms .form-container .form-footer .proceed').classList.add('hide');
    document.querySelector('.forms .form-container .form-footer .links').classList.remove('hide');
    proceedToStep2();
}

document.querySelector('#btn-cart-step-1').addEventListener('click', function () {
    if (!this.className.includes('disabled')) {
        proceedToStep2();
        updateFormArray();
    }
});

document.querySelector('#btn-cart-step-2').addEventListener('click', function () {
    if (!this.className.includes('disabled')) {
        updateFormArray();
        checkRequired();
    }
})

document.querySelector('.payment-item-3 a').addEventListener('click', function (evt) {
    evt.preventDefault();
    enableForm();
    returnToStep2();
})

document.querySelector('#btn-cart-step-3').addEventListener('click', function () {
    let paymentMethod = document.querySelector('#pay-method').value;
    if (paymentMethod != 'pay-method-null') {
        alert('Your order was made successfully! Thank you!');
        window.location.href = './index.html';
    } else {
        document.querySelector('#pay-method').classList.add('required');
    }
})

document.querySelector('#pay-method').addEventListener('change', function () {
    this.classList.remove('required');
    if (this.value === 'pay-method-null') this.classList.remove('selected');
    else this.classList.add('selected');
})

document.querySelector('#del-method').addEventListener('change', function () {
    this.classList.remove('required');
    if (this.value === 'del-method-null') this.classList.remove('selected');
    else this.classList.add('selected');
})

function updateFormArray() {
    let delivery = document.querySelector('#del-method').value;
    let firstName = document.querySelector('#first-name').value;
    let lastName = document.querySelector('#last-name').value;
    let addressOne = document.querySelector('#address-1').value;
    let addressTwo = document.querySelector('#address-2').value;
    let city = document.querySelector('#city').value;
    let postalCode = document.querySelector('#postal-code').value;
    let phoneNumber = document.querySelector('#phone-number').value;
    let email = document.querySelector('#email').value;
    let billing = document.querySelector('#for-billing').checked;
    formArray = [
        {
            id: '#del-method',
            value: delivery,
            required: true
        },
        {
            id: '#first-name',
            value: firstName,
            required: true
        },
        {
            id: '#last-name',
            value: lastName,
            required: true
        },
        {
            id: '#address-1',
            value: addressOne,
            required: true
        },
        {
            id: '#address-2',
            value: addressTwo,
            required: true
        },
        {
            id: '#city',
            value: city,
            required: true
        },
        {
            id: '#postal-code',
            value: postalCode,
            required: true
        },
        {
            id: '#phone-number',
            value: phoneNumber,
            required: true,
            regex: phoneRegex
        },
        {
            id: '#email',
            value: email,
            required: true,
            regex: mailRegex
        },
        {
            id: '#for-billing',
            value: billing,
            required: false
        }
    ]
}

function checkRequired() {
    formArray.forEach(function (item) {
        if (item.required && (!item.value || item.value === 'del-method-null')) {
            document.querySelector(item.id).classList.add('required');
        }
        else document.querySelector(item.id).classList.remove('required');
    })
    checkRegex();
    formValidator();
}

function checkRegex() {
    let regexInputs = formArray.filter(function (item) {
        return item.value && item.regex;
    })
    if (regexInputs.length > 0) {
        regexInputs.forEach(function (inp) {
            if (!inp.value.match(inp.regex)) {
                document.querySelector(inp.id).classList.add('required');
            } else {
                document.querySelector(inp.id).classList.remove('required');
            }
        })
    }
}

function formValidator() {
    let inputs = document.querySelectorAll('.input .required');
    if (inputs.length === 0) {
        disableForm();
        proceedToStep3();
    } else {
        document.querySelector('.item-list .list-container .list-footer .proceed').scrollIntoView();
    }
}

function disableForm() {
    formArray.forEach(function (item) {
        document.querySelector(item.id).setAttribute('disabled', 'disabled');
    })
}

function enableForm() {
    formArray.forEach(function (item) {
        document.querySelector(item.id).removeAttribute('disabled');
    })
}