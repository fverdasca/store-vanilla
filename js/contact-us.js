document.querySelector('#contact-btn').addEventListener('click', function () {
    updateContactArray();
    checkRequiredContact();
})

document.querySelector('#user-topic').addEventListener('change', function () {
    this.classList.remove('required');
    if (this.value === 'topic-null') this.classList.remove('selected');
    else this.classList.add('selected');
})

function updateContactArray() {
    let userTopic = document.querySelector('#user-topic').value;
    let userName = document.querySelector('#user-name').value;
    let userEmail = document.querySelector('#user-email').value;
    let userMessage = document.querySelector('#user-message').value;
    contactArray = [
        {
            id: '#user-topic',
            value: userTopic,
            required: true
        },
        {
            id: '#user-name',
            value: userName,
            required: true
        },
        {
            id: '#user-email',
            value: userEmail,
            required: true,
            regex: mailRegex
        },
        {
            id: '#user-message',
            value: userMessage,
            required: true,
        }
    ]
}

function checkRequiredContact() {
    contactArray.forEach(function (item) {
        if (item.required && (!item.value || item.value === 'topic-null')) {
            document.querySelector(item.id).classList.add('required');
        }
        else document.querySelector(item.id).classList.remove('required');
    })
    checkRegexContact();
    formValidatorContact();
}

function checkRegexContact() {
    let regexInputs = contactArray.filter(function (item) {
        return item.value && item.regex;
    })
    if (regexInputs.length > 0) {
        regexInputs.forEach(function (inp) {
            if (!inp.value.match(inp.regex)) {
                document.querySelector(inp.id).classList.add('required');
            } else {
                document.querySelector(inp.id).classList.remove('required');
            }
        })
    }
}

function formValidatorContact() {
    let inputs = document.querySelectorAll('.contact .required');
    if (inputs.length === 0) {
        alert('Message successfully sent! Thank you!')
    } else {
        alert('Something went wrong!')
    }
}