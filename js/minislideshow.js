let scrollStatus = 0;

document.querySelector('.mini-slideshow .left').addEventListener('click', function() {
	if (!this.classList.contains('disabled')) {
		let maxWidth = 300 * document.querySelector('.mini-slideshow .container').childElementCount;
		let containerWidth = document.querySelector('.mini-slideshow .container').offsetWidth;
		let remainingWidth = maxWidth - containerWidth;
		let scrollRatio = remainingWidth / 4;
		if (scrollStatus === 1) {
			document.querySelectorAll('.mini-slideshow .container .mini-photo').forEach(function(item) {
				item.style.left = '0px';
			});
			this.classList.add('disabled');
		} else if (scrollStatus === 2) {
			document.querySelectorAll('.mini-slideshow .container .mini-photo').forEach(function(item) {
				item.style.left = -1 * scrollRatio * 1 + 'px';
			});
		} else if (scrollStatus === 3) {
			document.querySelectorAll('.mini-slideshow .container .mini-photo').forEach(function(item) {
				item.style.left = -1 * scrollRatio * 2 + 'px';
			});
		} else if (scrollStatus === 4) {
			document.querySelectorAll('.mini-slideshow .container .mini-photo').forEach(function(item) {
				item.style.left = -1 * scrollRatio * 3 + 'px';
			});
		}
		scrollStatus -= 1;
		document.querySelector('.mini-slideshow .right').classList.remove('disabled');
	}
});

document.querySelector('.mini-slideshow .right').addEventListener('click', function() {
	if (!this.classList.contains('disabled')) {
		let maxWidth = 300 * document.querySelector('.mini-slideshow .container').childElementCount;
		let containerWidth = document.querySelector('.mini-slideshow .container').offsetWidth;
		let remainingWidth = maxWidth - containerWidth;
		let scrollRatio = remainingWidth / 4;

		if (scrollStatus === 0) {
			document.querySelectorAll('.mini-slideshow .container .mini-photo').forEach(function(item) {
				item.style.left = -1 * scrollRatio * 1 + 'px';
			});
		} else if (scrollStatus === 1) {
			document.querySelectorAll('.mini-slideshow .container .mini-photo').forEach(function(item) {
				item.style.left = -1 * scrollRatio * 2 + 'px';
			});
		} else if (scrollStatus === 2) {
			document.querySelectorAll('.mini-slideshow .container .mini-photo').forEach(function(item) {
				item.style.left = -1 * scrollRatio * 3 + 'px';
			});
		} else if (scrollStatus === 3) {
			document.querySelectorAll('.mini-slideshow .container .mini-photo').forEach(function(item) {
				item.style.left = -1 * scrollRatio * 4 + 'px';
			});
			this.classList.add('disabled');
		}
		scrollStatus += 1;
		document.querySelector('.mini-slideshow .left').classList.remove('disabled');
	}
});

document.querySelectorAll('.container .mini-photo').forEach(function(item) {
	item.addEventListener('click', function() {
		let att = item.getAttribute('style');
		att = att.substring(0, 132);
		console.log(att);
		document.querySelector('.mini-slideshow .overlay .image').setAttribute('style', att);
		document.querySelector('.mini-slideshow .overlay').style.display = 'block';
	});
});

document.querySelector('.overlay .image i').addEventListener('click', function() {
	document.querySelector('.mini-slideshow .overlay').style.display = 'none';
});
