let bundles = products.filter(function(item) {
	return item.bundle;
});

bundles.forEach(function(item) {
	let aTag = document.createElement('a');
	aTag.href = './product.html';
	let artTag = document.createElement('article');
	let imgTag = document.createElement('img');
	let descTag = document.createElement('p');
	descTag.classList.add('description');
	descTag.innerHTML = item.description;
	let priceTag = document.createElement('p');
	priceTag.classList.add('price');
	priceTag.innerHTML = item.price;
	imgTag.setAttribute('src', item.image);
	artTag.appendChild(imgTag);
	artTag.appendChild(descTag);
	artTag.appendChild(priceTag);
	aTag.appendChild(artTag);
	let typ = '.articles.' + item.type + ' .article-container.' + item.type;
	let more = '.articles.' + item.type + ' .article-container' + '.more-' + item.type;
	let container = document.querySelector(typ);
	let morecont = document.querySelector(more);
	if (container.childElementCount < 4) {
		container.appendChild(aTag);
	} else {
		morecont.appendChild(aTag);
	}
});

document.querySelectorAll('.hidden').forEach(function(item) {
	if (item.childElementCount === 0) {
		let it = item.parentElement.children;
		for (let tag of it) {
			if (tag.className.includes('show-more') || tag.className.includes('material-icons')) {
				tag.classList.add('hide');
			}
		}
	}
});

document.querySelector('.articles.gaming .show-more').addEventListener('click', function() {
	let icon = document.querySelector('.articles.gaming i');
	if (icon.className.includes('close')) {
		icon.classList.remove('close');
		this.innerHTML = 'show more';
		document.querySelectorAll('.articles.gaming .more-gaming').forEach(function(item) {
			item.classList.add('hidden');
		});
	} else {
		icon.classList.add('close');
		this.innerHTML = 'show less';
		document.querySelectorAll('.articles.gaming .more-gaming').forEach(function(item) {
			item.classList.remove('hidden');
		});
	}
});
document.querySelector('.articles.work .show-more').addEventListener('click', function() {
	let icon = document.querySelector('.articles.work i');
	if (icon.className.includes('close')) {
		icon.classList.remove('close');
		this.innerHTML = 'show more';
		document.querySelectorAll('.articles.work .more-work').forEach(function(item) {
			item.classList.add('hidden');
		});
	} else {
		icon.classList.add('close');
		this.innerHTML = 'show less';
		document.querySelectorAll('.articles.work .more-work').forEach(function(item) {
			item.classList.remove('hidden');
		});
	}
});
document.querySelector('.articles.social .show-more').addEventListener('click', function() {
	let icon = document.querySelector('.articles.social i');
	if (icon.className.includes('close')) {
		icon.classList.remove('close');
		this.innerHTML = 'show more';
		document.querySelectorAll('.articles.social .more-social').forEach(function(item) {
			item.classList.add('hidden');
		});
	} else {
		icon.classList.add('close');
		this.innerHTML = 'show less';
		document.querySelectorAll('.articles.social .more-social').forEach(function(item) {
			item.classList.remove('hidden');
		});
	}
});
